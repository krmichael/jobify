const dbConnection = require('../models/db')

module.exports = {
  async index (req, res) {
    const db = await dbConnection
    const categoriasDb = await db.all('select * from categorias;')
    const vagas = await db.all('select * from vagas;')

    const categorias = categoriasDb.map(cat => {
      return {
        ...cat,
        vagas: vagas.filter(vaga => vaga.categoria === vaga.id)
      }
    })

    res.render('home', { categorias })
  },

  async show (req, res) {
    const db = await dbConnection
    const { id } = req.params
    const vaga = await db.get(`select * from vagas where id = ${id}`)

    res.render('vaga', { vaga })
  }
}
