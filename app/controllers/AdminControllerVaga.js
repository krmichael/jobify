const dbConnection = require('../models/db')

module.exports = {
  async admin (req, res) {
    res.render('admin/home')
  },

  async index (req, res) {
    const db = await dbConnection
    const vagas = await db.all('select * from vagas')

    res.render('admin/vagas', { vagas })
  },

  async show (req, res) {
    const db = await dbConnection
    const { id } = req.params
    const vaga = await db.get(`select * from vagas where id = ${id}`)
    const categorias = await db.all('select * from categorias')

    res.render('admin/editar-vaga', { vaga, categorias })
  },

  async update (req, res) {
    const { categoria, titulo, descricao } = req.body
    const { id } = req.params
    const db = await dbConnection

    await db.run(
      `update vagas set
        categoria = ${categoria},
        titulo = '${titulo}',
        descricao = '${descricao}'
        where id = ${id}`
    )

    res.redirect('/admin/vagas')
  },

  async showNew (req, res) {
    const db = await dbConnection
    const categorias = await db.all('select * from categorias')

    res.render('admin/nova-vaga', { categorias })
  },

  async store (req, res) {
    const { categoria, titulo, descricao } = req.body
    const db = await dbConnection
    await db.run(
      `insert into vagas (categoria, titulo, descricao)
      values ('${categoria}', '${titulo}', '${descricao}')`
    )

    res.redirect('/admin/vagas')
  },

  async destroy (req, res) {
    const db = await dbConnection
    const { id } = req.params
    await db.run(`delete from vagas where id = ${id}`)

    res.redirect('/admin/vagas')
  }
}
