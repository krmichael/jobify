const app = require('./app/config/express')()

app.use('/', require('./app/routes'))

app.listen(3000, err => {
  if (err) {
    console.log('Error server Jobify')
  }
  console.log('Server Jobify running at http://localhost:3000')
})
